import { describe, it, expect, beforeEach } from "vitest";
import { MessageDecoder } from "./message-decoder";
import { AMessageBase } from "../entities/serial-messages/a-message-base";
import { GetRaceStatusResponse, FuelMode } from "../entities/serial-messages/get-race-status.response";
import { GetTimeResponse, MeasuringPoint } from "../entities/serial-messages/get-time-response";
import { GetVersionNumberResponse } from "../entities/serial-messages/get-version-number.response";
import { ResetLapAndPositionCounterResponse } from "../entities/serial-messages/reset-lap-and-position-counter.response";
import { StartRaceOrPaceCarButtonPressedResponse } from "../entities/serial-messages/start-race-or-pace-car-button-pressed.response";
import { StartRaceResponse } from "../entities/serial-messages/start-race.response";

describe("Message decoder", () => {
  let decoder: MessageDecoder;

  beforeEach(() => {
    decoder = new MessageDecoder();
  });

  it("should throw an error if message is to short", () => {
    expect(() => decoder.decodeMessage("T", 0)).toThrow;
  });

  it("should throw an error when trying to decode an unknown message", () => {
    expect(() => decoder.decodeMessage("invalid$", 0)).toThrow;
  });

  it("should throw an error when checksum does not match", () => {
    //TODO
  });

  it("should decode get version response", () => {
    const decoded: AMessageBase = decoder.decodeMessage("053372$", 0);

    expect(decoded.encodedData).toEqual("053372$");
    expect(decoded.timestamp).toEqual(0);
    expect(decoded).toBeInstanceOf(GetVersionNumberResponse);
    expect((decoded as GetVersionNumberResponse).versionNumber).toEqual(5337);
  });

  it("should decode reset round and position counter message", () => {
    const decoded: AMessageBase = decoder.decodeMessage("J$", 0);

    expect(decoded.encodedData).toEqual("J$");
    expect(decoded.timestamp).toEqual(0);
    expect(decoded).toBeInstanceOf(ResetLapAndPositionCounterResponse);
  });

  it("should decode start race or pacecar button pressed  message", () => {
    const decoded: AMessageBase = decoder.decodeMessage("T$", 0);

    expect(decoded.encodedData).toEqual("T$");
    expect(decoded.timestamp).toEqual(0);
    expect(decoded).toBeInstanceOf(StartRaceOrPaceCarButtonPressedResponse);
  });

  it("should decode start race message", () => {
    const decoded: AMessageBase = decoder.decodeMessage("=$", 0);

    expect(decoded.encodedData).toEqual("=$");
    expect(decoded.timestamp).toEqual(0);
    expect(decoded).toBeInstanceOf(StartRaceResponse);
  });

  it("should decode race status message if race is not active", () => {
    const decoded: AMessageBase = decoder.decodeMessage("?:0000000010008003$", 0);

    expect(decoded.encodedData).toEqual("?:0000000010008003$");
    expect(decoded.timestamp).toEqual(0);
    expect(decoded).toBeInstanceOf(GetRaceStatusResponse);
    expect((decoded as GetRaceStatusResponse).isRaceActive).toEqual(false);
    expect((decoded as GetRaceStatusResponse).carFuels[0]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).carFuels[1]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).carFuels[2]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).carFuels[3]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).carFuels[4]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).carFuels[5]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).signalLightsState).toEqual(1);
    expect((decoded as GetRaceStatusResponse).fuelMode).toEqual(FuelMode.noFuel);
  });

  it("should decode race status message if race is starting", () => {
    const decoded: AMessageBase = decoder.decodeMessage("?:0000000041008004$", 0);

    expect(decoded.encodedData).toEqual("?:0000000041008004$");
    expect(decoded.timestamp).toEqual(0);
    expect(decoded).toBeInstanceOf(GetRaceStatusResponse);
    expect((decoded as GetRaceStatusResponse).isRaceActive).toEqual(false);
    expect((decoded as GetRaceStatusResponse).carFuels[0]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).carFuels[1]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).carFuels[2]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).carFuels[3]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).carFuels[4]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).carFuels[5]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).signalLightsState).toEqual(4);
    expect((decoded as GetRaceStatusResponse).fuelMode).toEqual(FuelMode.normalMode);
  });

  it("should decode race status message if race is active", () => {
    const decoded: AMessageBase = decoder.decodeMessage("?:0000000002008004$", 0);

    expect(decoded.encodedData).toEqual("?:0000000002008004$");
    expect(decoded.timestamp).toEqual(0);
    expect(decoded).toBeInstanceOf(GetRaceStatusResponse);
    expect((decoded as GetRaceStatusResponse).isRaceActive).toEqual(true);
    expect((decoded as GetRaceStatusResponse).carFuels[0]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).carFuels[1]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).carFuels[2]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).carFuels[3]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).carFuels[4]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).carFuels[5]).toEqual(0);
    expect((decoded as GetRaceStatusResponse).signalLightsState).toEqual(0);
    expect((decoded as GetRaceStatusResponse).fuelMode).toEqual(FuelMode.realMode);
  });

  it("should decode car fuels", () => {
    const decoded: AMessageBase = decoder.decodeMessage("?:1234560002008004$", 0);

    expect(decoded.encodedData).toEqual("?:1234560002008004$");
    expect(decoded.timestamp).toEqual(0);
    expect(decoded).toBeInstanceOf(GetRaceStatusResponse);
    expect((decoded as GetRaceStatusResponse).isRaceActive).toEqual(true);
    expect((decoded as GetRaceStatusResponse).carFuels[0]).toEqual(1);
    expect((decoded as GetRaceStatusResponse).carFuels[1]).toEqual(2);
    expect((decoded as GetRaceStatusResponse).carFuels[2]).toEqual(3);
    expect((decoded as GetRaceStatusResponse).carFuels[3]).toEqual(4);
    expect((decoded as GetRaceStatusResponse).carFuels[4]).toEqual(5);
    expect((decoded as GetRaceStatusResponse).carFuels[5]).toEqual(6);
    expect((decoded as GetRaceStatusResponse).signalLightsState).toEqual(0);
    expect((decoded as GetRaceStatusResponse).fuelMode).toEqual(FuelMode.realMode);
  });

  it("should decode car fuel for second car", () => {
    expect(
      (decoder.decodeMessage("?:>>>>>>000600800<$", 0) as GetRaceStatusResponse).carFuels[1],
    ).toEqual(14);
    expect(
      (decoder.decodeMessage("?:>=>>>>000600800;$", 0) as GetRaceStatusResponse).carFuels[1],
    ).toEqual(13);
    expect(
      (decoder.decodeMessage("?:><>>>>000600800:$", 0) as GetRaceStatusResponse).carFuels[1],
    ).toEqual(12);
    expect(
      (decoder.decodeMessage("?:>;>>>>0006008009$", 0) as GetRaceStatusResponse).carFuels[1],
    ).toEqual(11);
    expect(
      (decoder.decodeMessage("?:>:>>>>0006008008$", 0) as GetRaceStatusResponse).carFuels[1],
    ).toEqual(10);
    expect(
      (decoder.decodeMessage("?:>9>>>>0006008007$", 0) as GetRaceStatusResponse).carFuels[1],
    ).toEqual(9);
    expect(
      (decoder.decodeMessage("?:>8>>>>0006008006$", 0) as GetRaceStatusResponse).carFuels[1],
    ).toEqual(8);
    expect(
      (decoder.decodeMessage("?:>7>>>>0006008005$", 0) as GetRaceStatusResponse).carFuels[1],
    ).toEqual(7);
    expect(
      (decoder.decodeMessage("?:>6>>>>0006008004$", 0) as GetRaceStatusResponse).carFuels[1],
    ).toEqual(6);
    expect(
      (decoder.decodeMessage("?:>5>>>>0006008003$", 0) as GetRaceStatusResponse).carFuels[1],
    ).toEqual(5);
    expect(
      (decoder.decodeMessage("?:>4>>>>0006008002$", 0) as GetRaceStatusResponse).carFuels[1],
    ).toEqual(4);
    expect(
      (decoder.decodeMessage("?:>3>>>>0006008001$", 0) as GetRaceStatusResponse).carFuels[1],
    ).toEqual(3);
    expect(
      (decoder.decodeMessage("?:>2>>>>0006008000$", 0) as GetRaceStatusResponse).carFuels[1],
    ).toEqual(2);
    expect(
      (decoder.decodeMessage("?:>1>>>>000600800?$", 0) as GetRaceStatusResponse).carFuels[1],
    ).toEqual(1);
    expect(
      (decoder.decodeMessage("?:>0>>>>000600800>$", 0) as GetRaceStatusResponse).carFuels[1],
    ).toEqual(0);
  });

  it("should throw an error when signal light is invalid", () => {
    expect(() => decoder.decodeMessage("?:0000000091008004$", 0)).toThrow();
    expect(() => decoder.decodeMessage("?:000000009x008004$", 0)).toThrow();
  });

  it("should throw an error when fuel mode invalid", () => {
    expect(() => decoder.decodeMessage("?:0000000044008004$", 0)).toThrow();
    expect(() => decoder.decodeMessage("?:000000004x008004$", 0)).toThrow();
  });

  it("should decode time for the first car measured on start/finish", () => {
    const decoded: AMessageBase = decoder.decodeMessage("?1000003:111$", 0);

    expect(decoded.encodedData).toEqual("?1000003:111$");
    expect(decoded.timestamp).toEqual(0);
    expect(decoded).toBeInstanceOf(GetTimeResponse);
    expect((decoded as GetTimeResponse).carId).toEqual(1);
    expect((decoded as GetTimeResponse).currentTime).toEqual(12314);
    expect((decoded as GetTimeResponse).measuringPoint).toEqual(MeasuringPoint.startFinish);
  });

  it("should decode time for the second car measured on start/finish", () => {
    const decoded: AMessageBase = decoder.decodeMessage("?20000<4551=$", 0);

    expect(decoded.encodedData).toEqual("?20000<4551=$");
    expect(decoded.timestamp).toEqual(0);
    expect(decoded).toBeInstanceOf(GetTimeResponse);
    expect((decoded as GetTimeResponse).carId).toEqual(2);
    expect((decoded as GetTimeResponse).currentTime).toEqual(19541);
    expect((decoded as GetTimeResponse).measuringPoint).toEqual(MeasuringPoint.startFinish);
  });

  it("should decode time for the second car measured on a checklane", () => {
    const decoded: AMessageBase = decoder.decodeMessage("?30000:8702<$", 0);

    expect(decoded.encodedData).toEqual("?30000:8702<$");
    expect(decoded.timestamp).toEqual(0);
    expect(decoded).toBeInstanceOf(GetTimeResponse);
    expect((decoded as GetTimeResponse).carId).toEqual(3);
    expect((decoded as GetTimeResponse).currentTime).toEqual(35335);
    expect((decoded as GetTimeResponse).measuringPoint).toEqual(MeasuringPoint.checkLane1);
  });

  it("should throw an error when car id is invalid", () => {
    expect(() => decoder.decodeMessage("?90000:8702<$", 0)).toThrow();
    expect(() => decoder.decodeMessage("?x0000:8702<$", 0)).toThrow();
  });

  it("should throw an error when measuring point is invalid", () => {
    expect(() => decoder.decodeMessage("?90000:8704<$", 0)).toThrow();
    expect(() => decoder.decodeMessage("?90000:870x<$", 0)).toThrow();
  });
});
