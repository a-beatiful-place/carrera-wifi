import * as dotenv from "dotenv";
import { getEnvValue } from "utils";

export type LogLevel = "fatal" | "error" | "warn" | "info" | "debug" | "trace";
export type Mode = "demo" | "live";

export type GeneralConfig = {
  logLevel: LogLevel;
  mode: Mode;
};

export type MqttConfig = {
  host: string;
  encodedTopic: string;
  publishTopicPrefix: string;
};

export type Database = {
  storageFile: string;
  demoFile: string;
};

export type Config = {
  general: GeneralConfig;
  mqtt: MqttConfig;
  database: Database;
};

export function getConfig(): Config {
  dotenv.config();

  return {
    general: {
      logLevel: getEnvValue("LOG_LEVEL", "info") as LogLevel,
      mode: getEnvValue("MODE", "demo") as Mode,
    },
    mqtt: {
      host: getEnvValue("MQTT_HOST", "192.168.2.150"),
      encodedTopic: getEnvValue("ENCODED_TOPIC", "Home/carrera/track/Encoded"),
      publishTopicPrefix: getEnvValue("PUBLISH_TOPIC_PREFIX", "Home/carrera"),
    },
    database: {
      storageFile: getEnvValue("STORAGE_FILE", "db.json"),
      demoFile: getEnvValue("DEMO_FILE", "demo-db.json"),
    },
  };
}
