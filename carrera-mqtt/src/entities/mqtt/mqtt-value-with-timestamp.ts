import { MqttValue } from "./mqtt-value";

export class MqttValueWithTimestamp extends MqttValue {
  public constructor(
    public readonly timeStamp: number,
    topicSuffix: string,
    value: string,
  ) {
    super(topicSuffix, value);
  }
}
