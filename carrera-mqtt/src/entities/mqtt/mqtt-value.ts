export class MqttValue {
  public constructor(
    public readonly topicSuffix: string,
    public readonly value: string,
  ) {}
}
