import { MqttValue } from "../../entities/mqtt/mqtt-value";
import { AMessageWithUpdates } from "../../entities/serial-messages/a-message-with-updates";

export abstract class AUpdateableModel {
  public abstract getCurrentValues(): MqttValue[];
  public abstract update(message: AMessageWithUpdates): MqttValue[];
  public abstract reset(): void;
}
