import { describe, it, expect, beforeEach } from "vitest";
import { RaceManager } from "./race-manager";
import { MqttValue } from "../entities/mqtt/mqtt-value";
import { GetRaceStatusResponse, FuelMode } from "../entities/serial-messages/get-race-status.response";
import { GetTimeResponse } from "../entities/serial-messages/get-time-response";

const initialMqttMessages: MqttValue[] = [
  new MqttValue("Race/1/SignalLightState", ""),
  new MqttValue("Race/1/FuelMode", ""),
  new MqttValue("Race/1/IsRaceActive", "0"),
  new MqttValue("Race/1/FastestCar", "0"),
  new MqttValue("Race/1/FastestLapTime", "0"),
  new MqttValue("Race/1/LeadingCar", "0"),

  new MqttValue("Car/1/NumberOfLaps", "0"),
  new MqttValue("Car/1/LastLapTime", "0"),
  new MqttValue("Car/1/TimeOfFastestLap", "0"),
  new MqttValue("Car/1/NumberOfFastestLap", "0"),

  new MqttValue("Car/2/NumberOfLaps", "0"),
  new MqttValue("Car/2/LastLapTime", "0"),
  new MqttValue("Car/2/TimeOfFastestLap", "0"),
  new MqttValue("Car/2/NumberOfFastestLap", "0"),

  new MqttValue("Car/3/NumberOfLaps", "0"),
  new MqttValue("Car/3/LastLapTime", "0"),
  new MqttValue("Car/3/TimeOfFastestLap", "0"),
  new MqttValue("Car/3/NumberOfFastestLap", "0"),

  new MqttValue("Car/4/NumberOfLaps", "0"),
  new MqttValue("Car/4/LastLapTime", "0"),
  new MqttValue("Car/4/TimeOfFastestLap", "0"),
  new MqttValue("Car/4/NumberOfFastestLap", "0"),

  new MqttValue("Car/5/NumberOfLaps", "0"),
  new MqttValue("Car/5/LastLapTime", "0"),
  new MqttValue("Car/5/TimeOfFastestLap", "0"),
  new MqttValue("Car/5/NumberOfFastestLap", "0"),

  new MqttValue("Car/6/NumberOfLaps", "0"),
  new MqttValue("Car/6/LastLapTime", "0"),
  new MqttValue("Car/6/TimeOfFastestLap", "0"),
  new MqttValue("Car/6/NumberOfFastestLap", "0"),

  new MqttValue("Car/ghostcar/NumberOfLaps", "0"),
  new MqttValue("Car/ghostcar/LastLapTime", "0"),
  new MqttValue("Car/ghostcar/TimeOfFastestLap", "0"),
  new MqttValue("Car/ghostcar/NumberOfFastestLap", "0"),

  new MqttValue("Car/pacecar/NumberOfLaps", "0"),
  new MqttValue("Car/pacecar/LastLapTime", "0"),
  new MqttValue("Car/pacecar/TimeOfFastestLap", "0"),
  new MqttValue("Car/pacecar/NumberOfFastestLap", "0"),
];

describe("race manager", () => {
  describe("initial values", () => {
    const raceManager: RaceManager = new RaceManager();

    it("should generate initial mqtt values", () => {
      const mqttMessages: MqttValue[] = raceManager.resetAndGetInitialValues();

      expect(mqttMessages).toEqual(initialMqttMessages);
    });
  });

  describe("update race status", () => {
    const raceManager: RaceManager = new RaceManager();

    it("should not have an active race when receiving race update", () => {
      expect(
        raceManager.update(
          new GetRaceStatusResponse(
            "does not matter",
            0,
            [0, 0, 0, 0, 0, 0, 0, 0],
            false,
            0,
            FuelMode.noFuel,
          ),
        ),
      ).toEqual([
        new MqttValue("Race/1/FuelMode", "0"),
        new MqttValue("Race/1/SignalLightState", "0"),
        new MqttValue("Race/1/IsRaceActive", "0"),
      ]);
      expect(raceManager.race.fuelMode).toEqual(FuelMode.noFuel);
      expect(raceManager.race.signalLightState).toEqual(0);
      expect(raceManager.race.isRaceActive).toEqual(false);
    });

    it("should start race when receiving a message with active race", () => {
      expect(
        raceManager.update(
          new GetRaceStatusResponse(
            "does not matter",
            0,
            [0, 0, 0, 0, 0, 0, 0, 0],
            true,
            0,
            FuelMode.noFuel,
          ),
        ),
      ).toEqual([new MqttValue("Race/1/IsRaceActive", "1")]);
      expect(raceManager.race.fuelMode).toEqual(FuelMode.noFuel);
      expect(raceManager.race.signalLightState).toEqual(0);
      expect(raceManager.race.isRaceActive).toEqual(true);
    });

    it("should update signal light state", () => {
      expect(
        raceManager.update(
          new GetRaceStatusResponse(
            "does not matter",
            0,
            [0, 0, 0, 0, 0, 0, 0, 0],
            true,
            1,
            FuelMode.noFuel,
          ),
        ),
      ).toEqual([new MqttValue("Race/1/SignalLightState", "1")]);
      expect(raceManager.race.fuelMode).toEqual(FuelMode.noFuel);
      expect(raceManager.race.signalLightState).toEqual(1);
      expect(raceManager.race.isRaceActive).toEqual(true);
    });

    it("should update fuel mode", () => {
      expect(
        raceManager.update(
          new GetRaceStatusResponse(
            "does not matter",
            0,
            [0, 0, 0, 0, 0, 0, 0, 0],
            true,
            1,
            FuelMode.normalMode,
          ),
        ),
      ).toEqual([new MqttValue("Race/1/FuelMode", "1")]);
      expect(raceManager.race.fuelMode).toEqual(FuelMode.normalMode);
      expect(raceManager.race.signalLightState).toEqual(1);
      expect(raceManager.race.isRaceActive).toEqual(true);
    });
  });

  describe("lap times and number of laps", () => {
    const raceManager: RaceManager = new RaceManager();

    it("should not generate updates when car 1 crosses line after 1000ms", () => {
      expect(raceManager.update(new GetTimeResponse("car 1 crosses line", 0, 1, 1000, 1))).toEqual(
        [],
      );
      expect(raceManager.race.cars[0]!.lastMeasuredTime).toEqual(1000);
      expect(raceManager.race.cars[0]!.lastLapTime).toBe(undefined);
      expect(raceManager.race.cars[0]!.numberOfLaps).toEqual(0);
      expect(raceManager.race.cars[0]!.fastestLapTime).toBe(undefined);
      expect(raceManager.race.cars[0]!.fastestLapNumber).toBe(undefined);

      expect(raceManager.race.cars[6]!.lastMeasuredTime).toBe(undefined);
      expect(raceManager.race.cars[6]!.numberOfLaps).toEqual(0);
      expect(raceManager.race.cars[6]!.lastLapTime).toBe(undefined);
      expect(raceManager.race.cars[6]!.fastestLapTime).toBe(undefined);
      expect(raceManager.race.cars[6]!.fastestLapNumber).toBe(undefined);

      expect(raceManager.race.fastestCar).toBe(undefined);
      expect(raceManager.race.fastestLapTime).toBe(undefined);
      expect(raceManager.race.leadingCar).toBe(undefined);
    });

    it("should not generate updates when car 7 crosses line after 2000ms", () => {
      expect(raceManager.update(new GetTimeResponse("car 7 crosses line", 0, 7, 2000, 1))).toEqual(
        [],
      );
      expect(raceManager.race.cars[0]!.lastMeasuredTime).toEqual(1000);
      expect(raceManager.race.cars[0]!.lastLapTime).toBe(undefined);
      expect(raceManager.race.cars[0]!.numberOfLaps).toEqual(0);
      expect(raceManager.race.cars[0]!.fastestLapTime).toBe(undefined);
      expect(raceManager.race.cars[0]!.fastestLapNumber).toBe(undefined);

      expect(raceManager.race.cars[6]!.lastMeasuredTime).toEqual(2000);
      expect(raceManager.race.cars[6]!.numberOfLaps).toEqual(0);
      expect(raceManager.race.cars[6]!.lastLapTime).toBe(undefined);
      expect(raceManager.race.cars[6]!.fastestLapTime).toBe(undefined);
      expect(raceManager.race.cars[6]!.fastestLapNumber).toBe(undefined);

      expect(raceManager.race.fastestCar).toBe(undefined);
      expect(raceManager.race.fastestLapTime).toBe(undefined);
      expect(raceManager.race.leadingCar).toBe(undefined);
    });

    it("should have car 1 with the fastest lap and as the leading car when car 1 crossing the line a second time after 5000ms", () => {
      expect(
        raceManager.update(
          new GetTimeResponse("car 1 crosses line the second time", 0, 1, 5000, 1),
        ),
      ).toEqual([
        new MqttValue("Car/1/NumberOfLaps", "1"),
        new MqttValue("Car/1/LastLapTime", "4000"),
        new MqttValue("Car/1/TimeOfFastestLap", "4000"),
        new MqttValue("Car/1/NumberOfFastestLap", "1"),
        new MqttValue("Race/1/FastestCar", "1"),
        new MqttValue("Race/1/FastestLapTime", "4000"),
        new MqttValue("Race/1/LeadingCar", "1"),
      ]);
      expect(raceManager.race.cars[0]!.lastMeasuredTime).toEqual(5000);
      expect(raceManager.race.cars[0]!.lastLapTime).toEqual(4000);
      expect(raceManager.race.cars[0]!.numberOfLaps).toEqual(1);
      expect(raceManager.race.cars[0]!.fastestLapTime).toEqual(4000);
      expect(raceManager.race.cars[0]!.fastestLapNumber).toEqual(1);

      expect(raceManager.race.cars[6]!.lastMeasuredTime).toEqual(2000);
      expect(raceManager.race.cars[6]!.numberOfLaps).toEqual(0);
      expect(raceManager.race.cars[6]!.lastLapTime).toBe(undefined);
      expect(raceManager.race.cars[6]!.fastestLapTime).toBe(undefined);
      expect(raceManager.race.cars[6]!.fastestLapNumber).toBe(undefined);

      expect(raceManager.race.fastestCar).not.toBe(undefined);
      expect(raceManager.race.fastestCar!.id).toEqual(1);
      expect(raceManager.race.fastestLapTime).toEqual(4000);
      expect(raceManager.race.leadingCar).not.toBe(undefined);
      expect(raceManager.race.leadingCar!.id).toEqual(1);
    });

    it("should have also car 1 with the fastest lap and the leading car when car 7 crossing the line a second time after 6000ms", () => {
      expect(
        raceManager.update(
          new GetTimeResponse("car 7 crosses line the second time", 0, 7, 6000, 1),
        ),
      ).toEqual([
        new MqttValue("Car/ghostcar/NumberOfLaps", "1"),
        new MqttValue("Car/ghostcar/LastLapTime", "4000"),
        new MqttValue("Car/ghostcar/TimeOfFastestLap", "4000"),
        new MqttValue("Car/ghostcar/NumberOfFastestLap", "1"),
      ]);
      expect(raceManager.race.cars[0]!.lastMeasuredTime).toEqual(5000);
      expect(raceManager.race.cars[0]!.lastLapTime).toEqual(4000);
      expect(raceManager.race.cars[0]!.numberOfLaps).toEqual(1);
      expect(raceManager.race.cars[0]!.fastestLapTime).toEqual(4000);
      expect(raceManager.race.cars[0]!.fastestLapNumber).toEqual(1);

      expect(raceManager.race.cars[6]!.lastMeasuredTime).toEqual(6000);
      expect(raceManager.race.cars[6]!.numberOfLaps).toEqual(1);
      expect(raceManager.race.cars[6]!.lastLapTime).toEqual(4000);
      expect(raceManager.race.cars[6]!.fastestLapTime).toEqual(4000);
      expect(raceManager.race.cars[6]!.fastestLapNumber).toEqual(1);

      expect(raceManager.race.fastestCar).not.toBe(undefined);
      expect(raceManager.race.fastestCar!.id).toEqual(1);
      expect(raceManager.race.fastestLapTime).toEqual(4000);
      expect(raceManager.race.leadingCar).not.toBe(undefined);
      expect(raceManager.race.leadingCar!.id).toEqual(1);
    });

    it("should have car 7 with the fastest lap and leading car when crossing the line a third time after 8000ms", () => {
      expect(
        raceManager.update(new GetTimeResponse("car 7 crosses line the third time", 0, 7, 8000, 1)),
      ).toEqual([
        new MqttValue("Car/ghostcar/NumberOfLaps", "2"),
        new MqttValue("Car/ghostcar/LastLapTime", "2000"),
        new MqttValue("Car/ghostcar/TimeOfFastestLap", "2000"),
        new MqttValue("Car/ghostcar/NumberOfFastestLap", "2"),
        new MqttValue("Race/1/FastestCar", "7"),
        new MqttValue("Race/1/FastestLapTime", "2000"),
        new MqttValue("Race/1/LeadingCar", "7"),
      ]);
      expect(raceManager.race.cars[0]!.lastMeasuredTime).toEqual(5000);
      expect(raceManager.race.cars[0]!.lastLapTime).toEqual(4000);
      expect(raceManager.race.cars[0]!.numberOfLaps).toEqual(1);
      expect(raceManager.race.cars[0]!.fastestLapTime).toEqual(4000);
      expect(raceManager.race.cars[0]!.fastestLapNumber).toEqual(1);

      expect(raceManager.race.cars[6]!.lastMeasuredTime).toEqual(8000);
      expect(raceManager.race.cars[6]!.numberOfLaps).toEqual(2);
      expect(raceManager.race.cars[6]!.lastLapTime).toEqual(2000);
      expect(raceManager.race.cars[6]!.fastestLapTime).toEqual(2000);
      expect(raceManager.race.cars[6]!.fastestLapNumber).toEqual(2);

      expect(raceManager.race.fastestCar).not.toBe(undefined);
      expect(raceManager.race.fastestCar!.id).toEqual(7);
      expect(raceManager.race.fastestLapTime).toEqual(2000);
      expect(raceManager.race.leadingCar).not.toBe(undefined);
      expect(raceManager.race.leadingCar!.id).toEqual(7);
    });
  });

  describe("reseting race because of status message", () => {
    const raceManager: RaceManager = new RaceManager();

    it("should have initial values", () => {
      expect(
        raceManager.update(
          new GetRaceStatusResponse(
            "does not matter",
            0,
            [0, 0, 0, 0, 0, 0, 0, 0],
            false,
            0,
            FuelMode.noFuel,
          ),
        ),
      ).toEqual([
        new MqttValue("Race/1/FuelMode", "0"),
        new MqttValue("Race/1/SignalLightState", "0"),
        new MqttValue("Race/1/IsRaceActive", "0"),
      ]);
    });

    it("should have an active race", () => {
      expect(
        raceManager.update(
          new GetRaceStatusResponse(
            "does not matter",
            0,
            [0, 0, 0, 0, 0, 0, 0, 0],
            true,
            0,
            FuelMode.noFuel,
          ),
        ),
      ).toEqual([new MqttValue("Race/1/IsRaceActive", "1")]);
    });

    it("should reset when race is stopped again", () => {
      const afterRaceStopped: MqttValue[] = [];

      afterRaceStopped.push(...initialMqttMessages);
      afterRaceStopped.push(new MqttValue("Race/1/FuelMode", "0"));
      afterRaceStopped.push(new MqttValue("Race/1/SignalLightState", "0"));
      afterRaceStopped.push(new MqttValue("Race/1/IsRaceActive", "0"));

      expect(
        raceManager.update(
          new GetRaceStatusResponse(
            "does not matter",
            0,
            [0, 0, 0, 0, 0, 0, 0, 0],
            false,
            0,
            FuelMode.noFuel,
          ),
        ),
      ).toEqual(afterRaceStopped);
    });
  });

  describe("resetting race because of invalid time measurements", () => {
    const raceManager: RaceManager = new RaceManager();

    it("should not generate updates when car 1 crosses line after 1000ms", () => {
      expect(raceManager.update(new GetTimeResponse("car 1 crosses line", 0, 1, 1000, 1))).toEqual(
        [],
      );
    });

    it("should generate reset updates when car 1 crosses line after 500ms", () => {
      expect(
        raceManager.update(
          new GetTimeResponse(
            "car 1 crosses line a second time with invalid time measurement",
            0,
            1,
            500,
            1,
          ),
        ),
      ).toEqual(initialMqttMessages);
    });
  });

  describe("car fuels", () => {
    const raceManager: RaceManager = new RaceManager();

    it("should not generate fuel updates when first setting car fuels", () => {
      expect(
        raceManager.update(
          new GetRaceStatusResponse("first fuels message", 0, [0, 0, 0, 0, 0, 0, 0, 0], true, 0, 0),
        ),
      ).toEqual([
        new MqttValue("Race/1/FuelMode", "0"),
        new MqttValue("Race/1/SignalLightState", "0"),
        new MqttValue("Race/1/IsRaceActive", "1"),
      ]);
    });

    it("should not generate updates when first setting car fuels", () => {
      expect(
        raceManager.update(
          new GetRaceStatusResponse("first fuels message", 0, [1, 2, 3, 4, 5, 6, 7, 8], true, 0, 0),
        ),
      ).toEqual([
        new MqttValue("Car/1/Fuel", "1"),
        new MqttValue("Car/2/Fuel", "2"),
        new MqttValue("Car/3/Fuel", "3"),
        new MqttValue("Car/4/Fuel", "4"),
        new MqttValue("Car/5/Fuel", "5"),
        new MqttValue("Car/6/Fuel", "6"),
        new MqttValue("Car/ghostcar/Fuel", "7"),
        new MqttValue("Car/pacecar/Fuel", "8"),
      ]);
    });
  });

  describe("leading car", () => {
    const raceManager: RaceManager = new RaceManager();

    it("should not have a leading car when car two crosses line after 1000ms", () => {
      expect(raceManager.update(new GetTimeResponse("car 2 crosses line", 0, 2, 1000, 1))).toEqual(
        [],
      );

      expect(raceManager.race.leadingCar).toBe(undefined);
    });

    it("should not have a leading car when car one crosses line after 1500ms", () => {
      expect(raceManager.update(new GetTimeResponse("car 1 crosses line", 0, 1, 1500, 1))).toEqual(
        [],
      );

      expect(raceManager.race.leadingCar).toBe(undefined);
    });

    it("should have car two as leading car when car two crosses line after 2000ms", () => {
      expect(raceManager.update(new GetTimeResponse("car 2 crosses line", 0, 2, 2000, 1))).toEqual([
        new MqttValue("Car/2/NumberOfLaps", "1"),
        new MqttValue("Car/2/LastLapTime", "1000"),
        new MqttValue("Car/2/TimeOfFastestLap", "1000"),
        new MqttValue("Car/2/NumberOfFastestLap", "1"),
        new MqttValue("Race/1/FastestCar", "2"),
        new MqttValue("Race/1/FastestLapTime", "1000"),
        new MqttValue("Race/1/LeadingCar", "2"),
      ]);

      expect(raceManager.race.leadingCar?.id).toEqual(2);
    });

    it("should still have car two as leading car when car one crosses line after 3000ms", () => {
      expect(raceManager.update(new GetTimeResponse("car 1 crosses line", 0, 1, 3000, 1))).toEqual([
        new MqttValue("Car/1/NumberOfLaps", "1"),
        new MqttValue("Car/1/LastLapTime", "1500"),
        new MqttValue("Car/1/TimeOfFastestLap", "1500"),
        new MqttValue("Car/1/NumberOfFastestLap", "1"),
      ]);

      expect(raceManager.race.leadingCar?.id).toEqual(2);
    });

    it("should have car one as leading car when car one crosses line after 4500ms", () => {
      expect(raceManager.update(new GetTimeResponse("car 1 crosses line", 0, 1, 4500, 1))).toEqual([
        new MqttValue("Car/1/NumberOfLaps", "2"),
        new MqttValue("Car/1/LastLapTime", "1500"),
        new MqttValue("Race/1/LeadingCar", "1"),
      ]);

      expect(raceManager.race.leadingCar?.id).toEqual(1);
    });

    it("should still have car one as leading car when car three crosses line after 5000ms", () => {
      expect(raceManager.update(new GetTimeResponse("car 3 crosses line", 0, 3, 5000, 1))).toEqual(
        [],
      );

      expect(raceManager.race.leadingCar?.id).toEqual(1);
    });

    it("should still have car one as leading car when car three crosses line after 10000ms", () => {
      expect(raceManager.update(new GetTimeResponse("car 3 crosses line", 0, 3, 10000, 1))).toEqual(
        [
          new MqttValue("Car/3/NumberOfLaps", "1"),
          new MqttValue("Car/3/LastLapTime", "5000"),
          new MqttValue("Car/3/TimeOfFastestLap", "5000"),
          new MqttValue("Car/3/NumberOfFastestLap", "1"),
        ],
      );

      expect(raceManager.race.leadingCar?.id).toEqual(1);
    });

    it("should still have car one as leading car when car three crosses line after 15000ms", () => {
      expect(raceManager.update(new GetTimeResponse("car 3 crosses line", 0, 3, 15000, 1))).toEqual(
        [
          new MqttValue("Car/3/NumberOfLaps", "2"),
          new MqttValue("Car/3/LastLapTime", "5000"),
        ],
      );

      expect(raceManager.race.leadingCar?.id).toEqual(1);
    });

    it("should have car three as leading car when car three crosses line after 20000ms", () => {
      expect(raceManager.update(new GetTimeResponse("car 3 crosses line", 0, 3, 20000, 1))).toEqual(
        [
          new MqttValue("Car/3/NumberOfLaps", "3"),
          new MqttValue("Car/3/LastLapTime", "5000"),
          new MqttValue("Race/1/LeadingCar", "3"),
        ],
      );

      expect(raceManager.race.leadingCar?.id).toEqual(3);
    });
  });
});
