import { connectAsync, MqttClient } from "mqtt";
import { IHealthCheck, IGracefulShutdown, HealthState } from "utils";
import { MqttConfig } from "../config";
import { Logger } from "pino";
import { MqttValue } from "../entities/mqtt/mqtt-value";

export class DecodedMqttMessagesAdapter implements IHealthCheck, IGracefulShutdown {
  public name: string = "Decoded messages publisher";

  private mqttClient: MqttClient | undefined;

  public constructor(
    private readonly logger: Logger,
    private readonly mqttConfig: MqttConfig,
  ) {}

  public async connect(): Promise<void> {
    this.logger.debug("Connecting to broker...");
    this.mqttClient = await connectAsync("mqtt://" + this.mqttConfig.host);
    this.logger.debug("...connected to mqtt broker!");
  }

  public async check(): Promise<HealthState> {
    if (!this.mqttClient) {
      return HealthState.NotReady;
    }

    if (!this.mqttClient.connected) {
      return HealthState.Unhealthy;
    }

    return HealthState.Healthy;
  }

  public async shutdown(): Promise<void> {
    if (this.mqttClient) {
      await this.mqttClient.endAsync();
    }
  }

  public publishUpdates(updates: MqttValue[]): void {
    if (this.mqttClient) {
      for (const update of updates) {
        this.mqttClient.publish(
          `${this.mqttConfig.publishTopicPrefix}/${update.topicSuffix}`,
          update.value,
        );
      }
    } else {
      throw new Error("Client is not set, please call the connect method first");
    }
  }
}
