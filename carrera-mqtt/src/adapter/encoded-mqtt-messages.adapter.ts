import { MqttClient, connectAsync } from "mqtt";
import { Logger } from "pino";
import { IHealthCheck, IGracefulShutdown, HealthState } from "utils";
import { MqttConfig } from "../config";
import { MqttValueWithTimestamp } from "../entities/mqtt/mqtt-value-with-timestamp";

export class EncodedMqttMessagesAdapter implements IHealthCheck, IGracefulShutdown {
  public name: string = "Encoded messages receiver";

  private mqttClient: MqttClient | undefined;

  public constructor(
    private readonly logger: Logger,
    private readonly mqttConfig: MqttConfig,
  ) {}

  public async connect(): Promise<void> {
    this.logger.debug("Connecting to broker...");
    this.mqttClient = await connectAsync("mqtt://" + this.mqttConfig.host);
    this.logger.debug("...connected to mqtt broker!");
  }

  public async check(): Promise<HealthState> {
    if (!this.mqttClient) {
      return HealthState.NotReady;
    }

    if (!this.mqttClient.connected) {
      return HealthState.Unhealthy;
    }

    return HealthState.Healthy;
  }

  public async shutdown(): Promise<void> {
    if (this.mqttClient) {
      await this.mqttClient.endAsync();
    }
  }

  public async subscribe(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      if (this.mqttClient) {
        this.mqttClient.subscribe(`${this.mqttConfig.encodedTopic}/#`, { qos: 1 }, (err) => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      } else {
        reject("Client is not set, please call the connect method first");
      }
    });
  }

  public registerOnMessageCallback(
    onValueReceived: (receivedValue: MqttValueWithTimestamp) => void,
  ): void {
    if (this.mqttClient) {
      this.mqttClient.on("message", (topic: string, message: Buffer) => {
        onValueReceived(new MqttValueWithTimestamp(+new Date(), topic, message.toString()));
      });
    } else {
      throw new Error("Client is not set, please call the connect method first");
    }
  }
}
