import lowDb from "lowdb";
import FileAsync from "lowdb/adapters/FileAsync";
import { MqttValueWithTimestamp } from "../entities/mqtt/mqtt-value-with-timestamp";
import { Logger } from "pino";
import { HealthState, IGracefulShutdown, IHealthCheck } from "utils";

type Schema = {
  receivedValues: MqttValueWithTimestamp[];
};

export class DatabaseAdapter implements IHealthCheck, IGracefulShutdown {
  public name: string = "database";

  private db: lowDb.LowdbAsync<Schema> | undefined;

  public constructor(
    private readonly logger: Logger,
    private readonly pathToDbFile: string = "db.json",
  ) {}

  public async shutdown(): Promise<void> {
    //nothing to do yet
  }

  public async check(): Promise<HealthState> {
    if (!this.db) {
      return HealthState.NotReady;
    }

    //TODO maybe implement something more meaningful
    return HealthState.Healthy;
  }

  public async init(): Promise<void> {
    const adapter = new FileAsync(this.pathToDbFile);
    this.db = await lowDb(adapter);

    this.db.defaults({ receivedValues: [] }).write();
  }

  public async addMqttValue(receivedValue: MqttValueWithTimestamp): Promise<void> {
    if (this.db) {
      this.logger.debug(`Storing value ${JSON.stringify(receivedValue)}...`);
      await this.db.get("receivedValues").push(receivedValue).write();
      this.logger.debug("... value stored");
    } else {
      throw new Error("DB is not initialized!");
    }
  }

  public async getAllValues(): Promise<MqttValueWithTimestamp[]> {
    if (this.db) {
      this.logger.debug(`Reading all values from file ${this.pathToDbFile}...`);
      const ret: MqttValueWithTimestamp[] = this.db.get("receivedValues").value();
      this.logger.debug(`... ${ret.length} values found.`);

      return ret;
    } else {
      throw new Error("DB is not initialized!");
    }
  }
}
