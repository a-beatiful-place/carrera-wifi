import { MessageDecoder } from "./decoder/message-decoder";
import { MqttValueWithTimestamp } from "./entities/mqtt/mqtt-value-with-timestamp";
import { RaceManager } from "./race-manager/race-manager";
import { MqttValue } from "./entities/mqtt/mqtt-value";
import { getConfig } from "./config";
import pino from "pino";
import { HealthCheckService, ShutdownHelper } from "utils";
import { DatabaseAdapter } from "./adapter/database.adapter";
import { DecodedMqttMessagesAdapter } from "./adapter/decoded-mqtt-messages.adapter";
import { EncodedMqttMessagesAdapter } from "./adapter/encoded-mqtt-messages.adapter";
import { AMessageBase } from "./entities/serial-messages/a-message-base";
import { AMessageWithUpdates } from "./entities/serial-messages/a-message-with-updates";

const logger = pino();
const config = getConfig();
logger.level = config.general.logLevel;

const shutdownHelper = new ShutdownHelper(logger);
const healthCheck = new HealthCheckService(logger, shutdownHelper, 10);

async function asyncMain(): Promise<void> {
  const databaseAdapter = new DatabaseAdapter(logger, config.database.storageFile);
  const encodedMessagesAdapter = new EncodedMqttMessagesAdapter(logger, config.mqtt);
  const decodedMessagesAdapter = new DecodedMqttMessagesAdapter(logger, config.mqtt);
  const decoder: MessageDecoder = new MessageDecoder();
  const raceManager: RaceManager = new RaceManager();

  let lastReceivedValue: MqttValueWithTimestamp | undefined;

  healthCheck.registerHealthCheck(databaseAdapter, encodedMessagesAdapter, decodedMessagesAdapter);
  shutdownHelper.registerForGracefulShutdown(
    databaseAdapter,
    encodedMessagesAdapter,
    decodedMessagesAdapter,
  );

  try {
    await databaseAdapter.init();
    await decodedMessagesAdapter.connect();
    await encodedMessagesAdapter.connect();
    await encodedMessagesAdapter.subscribe();
  } catch (e) {
    shutdownHelper.fatal("Unexpected error, exiting app", e);
  }

  encodedMessagesAdapter.registerOnMessageCallback((receivedValue: MqttValueWithTimestamp) => {
    if (!lastReceivedValue || lastReceivedValue.value !== receivedValue.value) {
      logger.debug(
        `Received a new value '${receivedValue.value} from topic '${receivedValue.topicSuffix}'`,
      );

      databaseAdapter.addMqttValue(receivedValue);
      lastReceivedValue = receivedValue;

      handleReceivedMqttMessage(receivedValue, decoder, raceManager, decodedMessagesAdapter);
    }
  });
}

function handleReceivedMqttMessage(
  receivedValue: MqttValueWithTimestamp,
  decoder: MessageDecoder,
  raceManager: RaceManager,
  mqttClient: DecodedMqttMessagesAdapter,
): void {
  try {
    const decoded: AMessageBase = decoder.decodeMessage(
      receivedValue.value,
      receivedValue.timeStamp,
    );

    if (decoded instanceof AMessageWithUpdates) {
      const updates: MqttValue[] = raceManager.update(decoded);

      mqttClient.publishUpdates(updates);
    }
  } catch (error) {
    console.error(error);
  }
}

async function runDemoFromDatabase(): Promise<void> {
  const databaseAdapter = new DatabaseAdapter(logger, config.database.demoFile);
  const decodedMessagesAdapter = new DecodedMqttMessagesAdapter(logger, config.mqtt);
  const decoder: MessageDecoder = new MessageDecoder();
  const raceManager: RaceManager = new RaceManager();

  healthCheck.registerHealthCheck(databaseAdapter, decodedMessagesAdapter);
  shutdownHelper.registerForGracefulShutdown(databaseAdapter, decodedMessagesAdapter);

  try {
    await databaseAdapter.init();
    await decodedMessagesAdapter.connect();
  } catch (e) {
    shutdownHelper.fatal("Unexpected error, exiting app", e);
  }

  const demoValues: MqttValueWithTimestamp[] = await databaseAdapter.getAllValues();

  logger.info(`Running demo with ${demoValues.length} values ...`);

  for (const demoValue of demoValues) {
    handleReceivedMqttMessage(demoValue, decoder, raceManager, decodedMessagesAdapter);
    await delay(100);
  }

  logger.info("... done");
}

async function delay(timeMs: number): Promise<void> {
  return new Promise<void>((resolve) => {
    setTimeout(resolve, timeMs);
  });
}

function run(mode: "live" | "demo") {
  if (mode === "live") {
    asyncMain().then(
      () => logger.info("RUNNING"),
      (error) => exit("Unexpected error in main", error),
    );
  } else if (mode === "demo") {
    runDemoFromDatabase().then(
      () => logger.info("RUNNING"),
      (error) => exit("Unexpected error in main", error),
    );
  }
}

function exit(message: string, error: any) {
  logger.fatal(message);
  logger.fatal(error);
  logger.fatal("Exiting...");
  process.exit();
}

run(config.general.mode);
