import { defineConfig } from "vitest/config";

export default defineConfig({
  test: {
    pool: "forks",
    coverage: {
      reporter: ["cobertura", "text"],
      exclude: [
        "**/{node_modules,dist,transform,test,routes}/**",
        "**/.{idea,git,cache,output,temp}/**",
        "**/{app,server,entities,logger,monitoring,dapr,swagger,crash-handler,typebox-extensions}.ts",
        "**/*.{entities,type,interface}.ts",
      ],
    },
  },
});
